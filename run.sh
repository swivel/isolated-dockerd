#!/usr/bin/env bash

# https://stackoverflow.com/a/246128
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

source $SCRIPT_DIR/vars.sh

mkdir -p $VENDOR
mkdir -p $HOME
mkdir -p $XDG_RUNTIME_DIR

chmod 0700 $XDG_RUNTIME_DIR
chmod 0700 $HOME

cleanup() {
	rm -rf $XDG_RUNTIME_DIR
}

trap "cleanup" 0

$SCRIPT_DIR/setup.sh

[[ -n "$VERBOSE" ]] && which slirp4netns
[[ -n "$VERBOSE" ]] && which rootlesskit

echo $DAEMON_START
$DAEMON_START


