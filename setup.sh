#!/usr/bin/env bash

# Digital Entertainment Corporation

# https://stackoverflow.com/a/246128
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

TMP=$SCRIPT_DIR/.tmp

source $SCRIPT_DIR/vars.sh

setup_dockerd() {
	CHANNEL="stable"
	STABLE_LATEST="20.10.6"

	STATIC_RELEASE_URL="https://download.docker.com/linux/static/$CHANNEL/$(uname -m)/docker-${STABLE_LATEST}.tgz"
	STATIC_RELEASE_ROOTLESS_URL="https://download.docker.com/linux/static/$CHANNEL/$(uname -m)/docker-rootless-extras-${STABLE_LATEST}.tgz"

	mkdir -p $TMP

	[[ ! -d $ROOTLESS ]] && {
		[[ ! -f $TMP/rootless.tgz ]] && curl -L -o $TMP/rootless.tgz "$STATIC_RELEASE_ROOTLESS_URL"
		mkdir -p $ROOTLESS
		tar -zxf "$TMP/rootless.tgz" -C $VENDOR/rootless --strip-components=1
	}

	[[ ! -d $DOCKER ]] && {
		[[ ! -f $TMP/docker.tgz ]] && curl -L -o $TMP/docker.tgz "$STATIC_RELEASE_URL" -O
		mkdir -p $DOCKER
		tar -zxf "$TMP/docker.tgz" -C $VENDOR/docker --strip-components=1
	}
}

setup_slirp() {
	[[ ! -d $SLIRP4NETNS ]] && {
		mkdir -p $SLIRP4NETNS
		SLIRP_RELEASE_URL='https://github.com/rootless-containers/slirp4netns/releases/download/v1.1.9/slirp4netns-x86_64'
		curl -L -o $SLIRP4NETNS/slirp4netns "$SLIRP_RELEASE_URL"
		chmod +x $SLIRP4NETNS/slirp4netns
	}
}

setup_uid_gid() {
	if [ "$EUID" -eq 0 ]; then
		echo "ERROR: Do NOT run this script as root."
		exit 1
	fi
	
	if ! command -v sudo &> /dev/null; then
		echo "ERROR: Must have sudo installed and configured"
		exit 1
	fi
	
	SVC_USER=$(whoami) #'game-ephesus-preq'
	
	# Ensure that User Namespaces is enabled
	unsEnabled=$(sysctl kernel.unprivileged_userns_clone)
	if [ "${unsEnabled: -1}" -eq 0 ]; then
		# If not, enable it
		$SUDO sysctl kernel.unprivileged_userns_clone=1
	fi
	
	# Create user account for game client
	#$SUDO useradd -r -s /bin/false $SVC_USER
	
	# If none exist for user, generate UIDs and GIDs for game client
	hasUids=$(grep "^$SVC_USER:" /etc/subuid 2>/dev/null)
	hasGids=$(grep "^$SVC_USER:" /etc/subgid 2>/dev/null)
	if [[ -z "$hasUids" || -z "$hasGids" ]]; then
		[[ ! -f /etc/subuid ]] && $SUDO touch /etc/subuid
		[[ ! -f /etc/subgid ]] && $SUDO touch /etc/subgid
	
		SUB_PREFIX=$(id -u) # 475 # DEC:ep
		$SUDO usermod \
			--add-subuids ${SUB_PREFIX}00000-${SUB_PREFIX}65536 \
			--add-subgids ${SUB_PREFIX}00000-${SUB_PREFIX}65536 \
			$SVC_USER
	fi
}

setup_dockerd
setup_slirp
setup_uid_gid
