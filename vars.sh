#!/usr/bin/env bash

# https://stackoverflow.com/a/246128
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

SUDO='sudo' # @TODO Change back to 'sudo' command

REAL_HOME=$HOME
REAL_XDG_RUNTIME_DIR=$XDG_RUNTIME_DIR

RUNTIME=$SCRIPT_DIR/.runtime
	HOME=$RUNTIME/home
	XDG_RUNTIME_DIR=$RUNTIME/run

VENDOR=$SCRIPT_DIR/.vendor
	DOCKER=$VENDOR/docker
	ROOTLESS=$VENDOR/rootless
	SLIRP4NETNS=$VENDOR/slirp4netns

DOCKER_HOST=unix://$XDG_RUNTIME_DIR/docker.sock
DOCKER_SOCKET=$DOCKER_HOST
DOCKER_BIN=$DOCKER/docker

DAEMON_START=$ROOTLESS/dockerd-rootless.sh

PATH=$DOCKER:$ROOTLESS:$SLIRP4NETNS:$PATH

# [[ -n "$VERBOSE" ]] && echo RUNTIME=$RUNTIME
# [[ -n "$VERBOSE" ]] && echo REAL_HOME=$REAL_HOME
# [[ -n "$VERBOSE" ]] && echo REAL_XDG_RUNTIME_DIR=$REAL_XDG_RUNTIME_DIR
# [[ -n "$VERBOSE" ]] && echo HOME=$HOME
# [[ -n "$VERBOSE" ]] && echo XDG_RUNTIME_DIR=$XDG_RUNTIME_DIR
# [[ -n "$VERBOSE" ]] && echo VENDOR=$VENDOR
# [[ -n "$VERBOSE" ]] && echo DOCKER=$DOCKER
# [[ -n "$VERBOSE" ]] && echo ROOTLESS=$ROOTLESS
# [[ -n "$VERBOSE" ]] && echo SLIRP4NETNS=$SLIRP4NETNS
# [[ -n "$VERBOSE" ]] && echo PATH=$PATH
# [[ -n "$VERBOSE" ]] && echo
# [[ -n "$VERBOSE" ]] && echo DOCKER_HOST=$DOCKER_HOST
# [[ -n "$VERBOSE" ]] && echo DOCKER_BIN=$DOCKER_BIN
# [[ -n "$VERBOSE" ]] && echo
# [[ -n "$VERBOSE" ]] && echo

